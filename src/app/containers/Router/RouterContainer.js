import React from 'react'
import { Route, Switch } from 'react-router-dom'
import FaqsContainer from 'containers/Faqs/FaqsContainer'
import HomeContainer from 'containers/Home/HomeContainer'
import { ROUTES } from 'constants/constants'

const Router = () => (
  <Switch>
    <Route component={HomeContainer} exact path={ROUTES.HOME} />
    <Route component={FaqsContainer} exact path={ROUTES.FAQS} />
  </Switch>
)

export default Router
