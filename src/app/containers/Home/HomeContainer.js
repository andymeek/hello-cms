import { graphql } from 'react-apollo'
import { branch, compose, mapProps, renderNothing } from 'recompose'
import gql from 'graphql-tag'

import Home from 'components/molecules/Home/Home'
import { ROUTES } from 'constants/constants'

const HOMEPAGES_QUERY = gql`
  query homepagesQuery {
    homepages {
      heading
      heroImageUrl
      subheading
    }
  }
`

const enhance = compose(
  graphql(HOMEPAGES_QUERY, {
    name: 'homepagesQuery',
  }),
  mapProps(({ homepagesQuery: { homepages } }) => {
    return {
      faqUrl: ROUTES.FAQS,
      heading: homepages && homepages[0].heading,
      heroImageUrl: homepages && homepages[0].heroImageUrl,
      subheading: homepages && homepages[0].subheading,
    }
  }),
  branch(props => !props, renderNothing)
)(Home)

export default enhance
