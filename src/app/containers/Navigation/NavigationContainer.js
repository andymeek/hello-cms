import React from 'react'
import Navigation from 'components/molecules/Navigation/Navigation'
import { ROUTES } from 'constants/constants'

const routes = Object.entries(ROUTES)

const NavigationContainer = () => <Navigation routes={routes} />

export default NavigationContainer
