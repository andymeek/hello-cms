import { graphql } from 'react-apollo'
import { branch, compose, mapProps, renderNothing, withStateHandlers } from 'recompose'
import gql from 'graphql-tag'

import Faqs from 'components/molecules/Faqs/Faqs'

const FAQS_QUERY = gql`
  query faqsQuery {
    faqs {
      body
      title
    }
  }
`
const enhance = compose(
  graphql(FAQS_QUERY, {
    name: 'faqsQuery',
  }),
  mapProps(({ faqsQuery }) => {
    return {
      faqs: faqsQuery.faqs && faqsQuery.faqs,
    }
  }),
  branch(({ faqs }) => !faqs, renderNothing),
  withStateHandlers(
    {
      activeFaq: 0,
    },
    {
      setFaq: () => index => {
        return {
          activeFaq: index,
        }
      },
    }
  )
)(Faqs)

export default enhance
