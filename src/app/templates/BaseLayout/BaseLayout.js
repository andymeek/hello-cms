import React, { Fragment } from 'react'

import Header from 'components/molecules/Header/Header'

const BaseLayout = ({ children }) => (
  <Fragment>
    <Header />
    {children}
  </Fragment>
)

export default BaseLayout
