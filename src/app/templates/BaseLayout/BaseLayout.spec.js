import React from 'react'
import { shallow } from 'enzyme'

import 'jest.setup'

import Header from 'components/molecules/Header/Header'
import BaseLayout from './BaseLayout'

describe('<BaseLayout />', () => {
  describe('default', () => {
    it('renders <Header /> component', () => {
      const wrapper = shallow(<BaseLayout />)

      expect(wrapper.find(Header)).toBeDefined()
    })

    it('renders its children', () => {
      const wrapper = shallow(
        <BaseLayout>
          <div />
        </BaseLayout>
      )

      expect(wrapper.find('div')).toHaveLength(1)
    })
  })
})
