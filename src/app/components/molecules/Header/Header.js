import React from 'react'
import NavigationContainer from 'containers/Navigation/NavigationContainer'

import logo from 'images/logo.png'

const Header = () => (
  <header className="header">
    <div className="logo">
      <img alt="Qantas" className="logo__src" src={logo} />
    </div>
    <NavigationContainer />
  </header>
)

export default Header
