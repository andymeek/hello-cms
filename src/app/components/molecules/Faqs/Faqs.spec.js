import React from 'react'
import { mount, shallow } from 'enzyme'
import 'jest.setup'
import Faqs from './Faqs'
import Nav from './atoms/Nav/Nav'

describe('<Faqs />', () => {
  let wrapper

  const defaultProps = {
    activeFaq: 0,
    faqs: [
      {
        body: 'Body 1',
        title: 'Title 1',
      },
    ],
    setFaq: () => {},
  }

  describe('default', () => {
    beforeAll(() => {
      wrapper = shallow(<Faqs {...defaultProps} />)
    })

    it('renders', () => {
      expect(wrapper).toHaveLength(1)
    })

    it('renders the <Nav />', () => {
      expect(wrapper.find(Nav)).toHaveLength(1)
    })

    it('passes down the prop activeFaq prop to <Nav />', () => {
      expect(wrapper.find(Nav).props().activeFaq).toEqual(defaultProps.activeFaq)
    })

    it('passes down the prop faqs prop to <Nav />', () => {
      expect(wrapper.find(Nav).props().faqs).toEqual(defaultProps.faqs)
    })

    it('passes down the prop setFaq prop to <Nav />', () => {
      expect(wrapper.find(Nav).props().setFaq).toEqual(defaultProps.setFaq)
    })
  })

  describe('activeFaq & faqs prop', () => {
    it('renders correct content', () => {
      wrapper = mount(<Faqs {...defaultProps} />)
      expect(wrapper.find('p').text()).toEqual(defaultProps.faqs[defaultProps.activeFaq].body)
    })
  })
})
