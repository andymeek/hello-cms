import React from 'react'
import PropTypes from 'prop-types'

import Nav from './atoms/Nav/Nav'

const Faqs = ({ activeFaq, faqs, setFaq }) => (
  <div className="container container--faqs">
    <h1>FAQs</h1>
    <div className="faqs">
      <div className="faqs__main">
        <p dangerouslySetInnerHTML={{ __html: faqs[activeFaq].body }} />
      </div>
      <aside className="faqs__nav">
        <Nav activeFaq={activeFaq} faqs={faqs} setFaq={setFaq} />
      </aside>
    </div>
  </div>
)

Faqs.propTypes = {
  activeFaq: PropTypes.number,
  faqs: PropTypes.arrayOf(PropTypes.object),
  setFaq: PropTypes.func,
}

export default Faqs
