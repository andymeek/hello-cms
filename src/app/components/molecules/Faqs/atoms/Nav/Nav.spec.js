import React from 'react'
import { mount } from 'enzyme'
import 'jest.setup'
import Nav from './Nav'

describe('<Nav />', () => {
  let wrapper

  const defaultProps = {
    activeFaq: 0,
    faqs: [
      {
        body: 'Body 1',
        title: 'Title 1',
      },
      {
        body: 'Body 2',
        title: 'Title 2',
      },
    ],
    setFaq: () => {},
  }

  beforeAll(() => {
    wrapper = mount(<Nav {...defaultProps} />)
  })

  describe('default', () => {
    it('renders', () => {
      expect(wrapper).toHaveLength(1)
    })
  })

  describe('activeFaq prop', () => {
    it('renders with the correct title', () => {
      expect(
        wrapper
          .find('div')
          .first()
          .text()
      ).toEqual(defaultProps.faqs[0].title)
    })

    it('renders the <div /> with the correct `faqs__link--active` className', () => {
      expect(
        wrapper
          .find('div')
          .first()
          .props().className
      ).toMatch('faqs__link--active')
    })

    it('only has the `faqs__link--active` className on the active <div /> ', () => {
      expect(
        wrapper
          .find('div')
          .last()
          .props().className
      ).not.toMatch('faqs__link--active')
    })
  })

  describe('faqs prop', () => {
    it('renders the correct amount of FAQs', () => {
      expect(wrapper.find('div')).toHaveLength(2)
    })
  })
})
