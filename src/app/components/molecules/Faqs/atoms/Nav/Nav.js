import React from 'react'
import classnames from 'classnames'
import PropTypes from 'prop-types'

const Nav = ({ activeFaq, faqs, setFaq }) =>
  faqs &&
  faqs.map(({ title }, index) => {
    return (
      <div
        className={classnames('faqs__link', activeFaq === index && 'faqs__link--active')}
        key={title}
        onClick={() => setFaq(index)}>
        {title}
      </div>
    )
  })

Nav.propTypes = {
  activeFaq: PropTypes.number,
  faqs: PropTypes.arrayOf(PropTypes.object),
  setFaq: PropTypes.func,
}

export default Nav
