import React from 'react'
import { NavLink } from 'react-router-dom'
import PropTypes from 'prop-types'

const Home = ({ faqUrl, heading, heroImageUrl, subheading }) => {
  return (
    <div className="container home">
      <h1>{heading}</h1>
      <h2 className="home__subheading">{subheading}</h2>
      <img alt={heading} className="home__hero" src={heroImageUrl} />
      <NavLink className="home__link" exact to={faqUrl}>
        Learn more
      </NavLink>
    </div>
  )
}

Home.propTypes = {
  faqUrl: PropTypes.string,
  heading: PropTypes.string,
  heroImageUrl: PropTypes.string,
  subheading: PropTypes.string,
}

export default Home
