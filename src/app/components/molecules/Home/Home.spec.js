import React from 'react'
import { NavLink } from 'react-router-dom'
import { shallow } from 'enzyme'

import 'jest.setup'

import Home from './Home'

describe('<Home />', () => {
  let wrapper

  const defaultProps = {
    faqUrl: '/faqs',
    heading: 'heading',
    heroImageUrl: 'heroImageUrl',
    subheading: 'subheading',
  }

  beforeAll(() => {
    wrapper = shallow(<Home {...defaultProps} />)
  })

  describe('default', () => {
    it('renders', () => {
      expect(wrapper).toHaveLength(1)
    })
  })

  describe('faqUrl prop', () => {
    it('is passed to the <NavLink /> component', () => {
      expect(wrapper.find(NavLink).props().to).toEqual(defaultProps.faqUrl)
    })
  })

  describe('heading prop', () => {
    it('renders the heading', () => {
      expect(wrapper.find('h1').text()).toEqual(defaultProps.heading)
    })

    it('is passed to the <img /> alt', () => {
      expect(wrapper.find('img').props().alt).toEqual(defaultProps.heading)
    })
  })

  describe('subheading prop', () => {
    it('renders the subheading', () => {
      expect(wrapper.find('h2').text()).toEqual(defaultProps.subheading)
    })
  })

  describe('heroImageUrl prop', () => {
    it('renders the <img />', () => {
      expect(wrapper.find('img')).toHaveLength(1)
    })

    it('renders the correct image source', () => {
      expect(wrapper.find('img').props().src).toEqual(defaultProps.heroImageUrl)
    })
  })
})
