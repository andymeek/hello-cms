import React from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'

const Navigation = ({ routes }) => (
  <nav className="navigation">
    {routes.map(route => {
      const text = route[1]
      const url = route[0]
      return (
        <NavLink activeClassName="navigation__link--active" className="navigation__link" exact key={text} to={text}>
          {url}
        </NavLink>
      )
    })}
  </nav>
)

Navigation.propTypes = {
  routes: PropTypes.array,
}

export default Navigation
