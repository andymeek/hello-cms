import React from 'react'
import { shallow } from 'enzyme'
import { NavLink } from 'react-router-dom'

import 'jest.setup'

import Navigation from './Navigation'

describe('<Navigation />', () => {
  let wrapper

  const defaultProps = {
    routes: [['/faqs', 'FAQS'], ['/', 'HOME']],
  }

  beforeAll(() => {
    wrapper = shallow(<Navigation {...defaultProps} />)
  })

  describe('default', () => {
    it('renders', () => {
      expect(wrapper).toHaveLength(1)
    })
  })

  describe('routes prop', () => {
    it('is passed to the <NavLink /> component', () => {
      expect(
        wrapper
          .find(NavLink)
          .first()
          .props().to
      ).toEqual(defaultProps.routes[0][1])
    })
  })
})
