import React from 'react'
import { ApolloClient } from 'apollo-client'
import { ApolloProvider } from 'react-apollo'
import { BrowserRouter } from 'react-router-dom'
import { createHttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'

import BaseLayout from 'templates/BaseLayout/BaseLayout'
import Router from 'containers/Router/RouterContainer'

const client = new ApolloClient({
  link: createHttpLink({ uri: process.env.REACT_APP_GRAPHQL_ENDPOINT }),
  cache: new InMemoryCache(),
})

const App = () => (
  <ApolloProvider client={client}>
    <BrowserRouter>
      <BaseLayout>
        <Router />
      </BaseLayout>
    </BrowserRouter>
  </ApolloProvider>
)

export default App
