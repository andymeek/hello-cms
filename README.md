# Hello CMS (developer challenge)

The following documentation is for Andy Meek's coding exercise submission for the front-end engineering position, and should provide an overview of the technology, practices and assumptions used to complete the task.

## Technology

I've chosen React as the library to complete this exercise because one of the objectives in the `hello-cms` challenge document specified that a SPA was required. React is good choice for this because it's a mature library with a expansive and vastly maintained ecosystem.

In order to get coding as soon as possible I've chosen to bootsrap the project using [Create React App](https://github.com/facebook/create-react-app) - with Jest, Babel, Webpack, linting, styling, etc configured and ready to use - i.e., minimal setup is required for a production ready web app.

I considered using [Next.js](https://github.com/zeit/next.js/) but opted against it due to it supporting server side rendering and I therefore came to the conclusion that this might be overkill for this task.

## GraphQL

GraphQL has been used to supply the CMS data. I wanted to experiment and have fun during this coding exercise, so I decided to use [Prisma](https://www.prisma.io/) as it provides a simple & free cloud-based GraphQL service. Through the command line and web app I was easily able to create a schema, import the data, and deploy a newly created service with the provided CMS JSON data in a short space of time (~30 mins). No need to create a custom server with resolvers etc :)

The [Schema](https://gitlab.com/andymeek/hello-cms/blob/master/src/prisma-service/datamodel.graphql) can be found in [here](https://gitlab.com/andymeek/hello-cms/blob/master/src/prisma-service/datamodel.graphql).

## Recompose

I chose the [Recompose library](https://github.com/acdlite/recompose) to `enhance` the React components. Recompose is a React utility belt for function components and higher-order components and encourages functional practices, e.g., favouring composition over OOP, and making testing much simpler!

## Design

- The hero image and the CMS JSON data provided were used for the content.
- The colour palette was taken from current Qantas Assure website.
- I didn't want to take the font files from the Qantas Assure website for copyright reasons so I chose a similar free one from [Google Fonts](https://fonts.google.com/specimen/Roboto?selection.family=Roboto)

## Styling

- SASS/SCSS was used as the pre-processor.
- [Reboot](https://github.com/twbs/bootstrap/blob/v4-dev/scss/_reboot.scss) was used to normalise the styling of the HTML elements.
- BEM methodology used.
- Idiomatic ordering (https://github.com/necolas/idiomatic-css#declaration-order) used for the CSS declarations.

## Assumptions

I decided upon designing the web app to be repsonsive using the following 2 breakpoints:

- Small viewport - 768 pixels and below.
- Large viewport - 769 pixels and above.

The app has only been developed for the latest version of Chrome.

## Folder structure

- The React components have been organised using [Atomic Design](http://bradfrost.com/blog/post/atomic-web-design/) - with business related JavaScript logic sitting inside the `containers` presentational components sitting inside `components`.

## Testing

- Jest is configured out of the box with Create React App so I simply added `enzyme-adapter-react-16` in order to unit test the React presentational components.

## Improvements

I'd make the following additions to improve this app if it was to be developed any further:

#### User experience

- Add a `url` property to each of the FAQs in the CMS data. This would allow each FAQ to be accessed directly from the router, enabling the user to navigate away and return back, or refresh the page without losing the currently selected FAQ.

#### Technologies

- Use Flow for type checking.
- CSS/SCSS modules rather than one large SCSS file.
- Display loading or skeletons when loading data asyncronously.

## Requirements

- Node LTS (version 8.11.3).

## Install

Installing the project dependencies...

`yarn` or `npm install`

## Run

Running the app locally in development mode...

`yarn start` or `npm start`

## Testing

Running the unit tests...

`yarn test` or `npm test`

## Format

Formatting the SCSS & JS...

`yarn format` or `npm format`

## Lint

Linting the SCSS...

`yarn lint:css` or `npm lint:css`

Note: linting of the JS comes out of the box with Create React App by starting the app in development mode (`yarn start` or `npm start`).

## Production

Firstly we need to build the assets...

`yarn build` or `npm build`

And finally, serve...

`yarn start:prod` or `npm start:prod`
